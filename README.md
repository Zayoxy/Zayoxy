# About me

- 💻 Learning computer science
- 🏠 Living in Switzerland
- 🗣 French/English

## Point of interests

- 🚀 Astronomy
- 📱 New technologies
- 👨‍💻 Programming
- 📶 Networking